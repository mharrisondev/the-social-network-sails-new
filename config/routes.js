/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝

  /* Auth Controller */
  'post /auth/login': 'AuthController.Login',
  'get /auth/refresh': 'AuthController.Refresh',
  'post /auth/recover-password': 'AuthController.PasswordResetRequest',
  'post /auth/new-password': 'AuthController.PasswordResetNew',
  /* Logout currently handled by front-end */

  /* User Controller */
  'post /auth/register': 'UserController.Create',
  'patch /user/details': 'UserController.UpdateDetails',
  'get /auth/user': 'UserController.Fetch',

  /* Avatar Controller */
  'post /user/avatar': 'AvatarController.UploadAvatar',
  'get /user/avatar/:id': 'AvatarController.DownloadAvatar',

  /* Status Controller */
  'post /user/status': 'StatusController.PostUserStatus',
  'get /user/status/:offset': 'StatusController.GetUserStatus',
  'patch /user/status': 'StatusController.EditUserStatus',
  'delete /user/status/:id': 'StatusController.DeleteUserStatus',
  'get /status/recent': 'StatusController.GetAllRecent'


  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝


};
