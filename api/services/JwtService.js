var jwt = require('jsonwebtoken');
var jwtSecret = sails.config.jwtSecret;

module.exports = {
  issue: function (payload) {
    sails.log(`Service:JwtService.issue()`);
    token = jwt.sign(payload, jwtSecret, {expiresIn: 180 * 60}); // 3 hours
    return token;
  },
  verify: function (token, callback) {
    sails.log(`Service:JwtService.verify()`);
    return jwt.verify(token, jwtSecret, callback);
  }
};
