/**
 * User.js
 *
 * @description :: User model definition.
 */

var bcrypt = require('bcrypt');

module.exports = {
  attributes: {
    Email: {
      type: 'string',
      required: true,
      unique: true,
      description: 'User email'
    },
    DisplayName: {
      type: 'string',
      required: true,
      unique: true,
      description: 'User display name (Handle, Alias)'
    },
    FirstName: {
      type: 'string',
      required: true,
      unique: false,
      description: 'User First Name'
    },
    LastName: {
      type: 'string',
      required: true,
      unique: false,
      description: 'User Last Name (Surname)'
    },
    About: {
      type: 'string',
      required: false,
      unique: false,
      description: 'A short description about the user'
    },
    Password: {
      type: 'string',
      minLength: 6,
      // protected: true,
      required: true,
      columnName: 'encryptedPassword'
      /* specify that the column name in the database for that particular
      attribute should be encryptedPassword and not the default password */
    },
    avatarUrl: {
      type: 'string',
      required: false,
      unique: true,
      description: 'Url for fetching user\'s Avatar (Display picture)'
    },
    avatarFd: {
      type: 'string',
      required: false,
      unique: true,
      description: 'Avatar file data, relative position on filesystem'
    },
    ResetPasswordToken: {
      type: 'string',
      required: false,
      description: 'Generated token for password reset integrity'
    },
    ResetPasswordTokenExpiry: {
      type: 'string',
      required: false,
      description: 'Password reset expiration date timestamp'
    }
  },
  customToJSON: function() {
    /* customToJSON used automatically each time user data is requested */
    /* Return a shallow copy of this record with the password removed. */
    return _.omit(this, 'Password');
  },
  beforeCreate: function(recordToInsert, cb){
    sails.log(`Model:User.beforeCreate()`);

    bcrypt.hash(recordToInsert.Password, 10, (err, hash) => {
      if (err) {
        sails.log('User.beforeCreate() bcrypt.hash() Error', err);
        return cb(err);
      }
      recordToInsert.Password = hash;
      sails.log('User.beforeCreate() bcrypt.hash()');
      cb(err, hash);
    });
  },
  comparePassword: function(password, user) {
    sails.log(`Model:User.comparePassword()`);

    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.Password, (err, match) => {
        if (err) {
          reject(err);
        }
        if (match) {
          resolve(true);
        } else {
          reject(err);
        }
      });
    });
  }
};
