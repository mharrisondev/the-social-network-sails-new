/**
 * Status.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    Message: {
      type: 'string',
      required: true,
      unique: false,
      description: 'Status message post'
    },
    Owner: {
      model: 'user',
      description: 'User who wrote status message'
    }
  },
};
