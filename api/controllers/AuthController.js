/**
 * AuthController
 *
 * @description :: Server-side actions for handling user authentication with JsonWebTokens.
 */

var crypto = require('crypto');
var bcrypt = require('bcrypt');

module.exports = {
  Login: async function (req, res) {
    sails.log(`AuthController.Login()`);

    var email = req.body.email;
    var password = req.body.password;

    verifyParams(res, email, password);

    var user = await User.findOne({'Email': email});

    if (user) {
      return signInUser(req, res, password, user);
    } else {
      return invalidEmailOrPassword(res);
    }
  },
  Refresh: function (req, res) {
    sails.log(`AuthController.Refresh()`);

    var responseData = {
      user: req.CurrentUser,
      token: generateToken(req.CurrentUser.id)
    };

    return ResponseService.json(200, res, 'Token Successfully refreshed', responseData);
  },
  PasswordResetRequest: async function (req, res) {
    sails.log(`AuthController.PasswordResetRequest()`);
    var email = req.body.email;

    var userRecord = await User.findOne({
      Email: email
    });

    if (!userRecord) {
      return ResponseService.json(401, res, 'Invalid email');
    }

    // generate token
    var token = crypto.randomBytes(32).toString('hex');

    sails.log('>>>> token check', token);
    sails.log('>>>> date test', Date.now() + 1*60*60*1000);

    await User.update({ id: userRecord.id }).set({
      ResetPasswordToken: token,
      ResetPasswordTokenExpiry: Date.now() + 24*60*60*1000 // 24 hours
    });

    // Send recovery email
    await sails.helpers.sendTemplateEmail.with({
      to: email,
      subject: 'Password reset instructions',
      template: 'email-reset-password',
      templateData: {
        token: token
      }
    });

    return ResponseService.json(200, res, 'Password reset requet successful');
  },
  PasswordResetNew: async function (req, res) {
    sails.log(`AuthController.PasswordResetNew()`);

    if (!req.body.token) {
      return ResponseService.json(422, res, 'Invalid data, please attempt password reset again');
    }

    if (req.body.input_password !== req.body.input_password_confirm) {
      return ResponseService.json(422, res, 'Provided passwords do not match, please try again');
    }

    var userRecord = await User.findOne({ ResetPasswordToken: req.body.token });

    if (!userRecord || userRecord.ResetPasswordTokenExpiry <= Date.now()) {
      return ResponseService.json(422, res, 'Invalid data, password reset has expired (24 hours), please try again');
    }

    // encrypt password
    var newPassword;
    await bcrypt.hash(req.body.input_password, 10).then((hash) => {
      newPassword = hash;
    });

    // update user record
    await User.update({ id: userRecord.id }, { Password: newPassword });

    return ResponseService.json(200, res, 'Successfully changed password');
  }
};

function signInUser(req, res, password, user) {
  sails.log(`AuthController signInUser()`);

  User.comparePassword(password, user).then(
    function (valid) {
      if (!valid) {
        return this.invalidEmailOrPassword();
      } else {
        var responseData = {
          user: user,
          token: generateToken(user.id)
        };
        return ResponseService.json(200, res, 'Successfully signed in', responseData);
      }
    }
  ).catch(err => {
    return ResponseService.json(403, res, `Forbidden: ${err}`);
  });
}

function invalidEmailOrPassword(res) {
  sails.log(`AuthController invalidEmailOrPassword()`);
  return ResponseService.json(401, res, 'Invalid email or password');
}

function verifyParams(res, email, password) {
  sails.log(`AuthController verifyParams()`);

  if (!email || !password) {
    return ResponseService.json(401, res, 'Email and password required');
  }
}

function generateToken(UserId) {
  sails.log(`AuthController generateToken()`);
  return JwtService.issue({id: UserId});
}
