/**
 * UserController
 *
 * @description :: Server-side actions for handling user related requests.
 */

module.exports = {
  Create: async function (req, res) {
    sails.log(`UserController.Create()`);

    if (req.body.password !== req.body.password_confirmation) {
      return ResponseService.json(401, res, 'Password doesn\'t match');
    }

    var data = {
      Email: req.body.email,
      DisplayName: req.body.display_name,
      FirstName: req.body.first_name,
      LastName: req.body.last_name,
      Password: req.body.password
    };

    try {
      await User.create(data).fetch();
      return ResponseService.json(200, res, 'User created successfully');
    } catch (error) {
      return ResponseService.json(500, res, 'User creation failed', error);
    }
  },
  Fetch: function (req, res) {
    sails.log(`UserController.Fetch()`);
    // default get request of vue-auth after successful $auth.login()

    var fetchData = {
      Email: req.CurrentUser.Email,
      DisplayName: req.CurrentUser.DisplayName,
      FirstName: req.CurrentUser.FirstName,
      LastName: req.CurrentUser.LastName,
      About: req.CurrentUser.About || null,
      id: req.CurrentUser.id
    };

    return ResponseService.json(200, res, 'User data fetch successful', fetchData);
  },
  UpdateDetails: async function (req, res) {
    sails.log(`UserController.UpdateDetails()`);
    var newData = {
      DisplayName: req.body.display_name,
      FirstName: req.body.first_name,
      LastName: req.body.last_name,
      About: req.body.about || ''
    };

    try {
      var updatedRecord = await User.update({'id': req.CurrentUser.id}, newData).fetch();
      return ResponseService.json(200, res, 'User details updated successfully', updatedRecord);
    } catch (error) {
      return ResponseService.json(500, res, 'User details update failed', error);
    }
  }
};
