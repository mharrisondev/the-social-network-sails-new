/**
 * AvatarController
 *
 * @description :: Server-side actions for handling avatar related requests.
 */

const aws = require('aws-sdk');
aws.config.region = 'us-west-1';

module.exports = {
  UploadAvatar: async function (req, res) {
    sails.log(`AvatarController.UploadAvatar()`);

    const s3 = new aws.S3();
    const fileName = req.query['file-name'];
    const fileType = req.query['file-type'];
    const s3Params = {
      Bucket: sails.config.S3_BUCKET_NAME,
      Key: sails.config.AWS_ACCESS_KEY_ID,
      Expires: 60,
      ContentType: fileType,
      ACL: 'public-read'
    };

    console.log('s3Params', s3Params);
    console.log('req.file(file)', req.file('file'));

    // req.file('file').upload({/* ~10MB */ maxBytes: 10000000}, (error, uploadedFiles) => {
    //   if (error) {
    //     return ResponseService.json(500, res, 'User avatar upload failed', error);
    //   }
    //
    //   if (uploadedFiles.length === 0){
    //     return ResponseService.json(400, res, 'No file was uploaded', error);
    //   }
    //
    //   // Save the file path and the url where the avatar for a user can be accessed
    //   User.update(req.CurrentUser.id, {
    //     // Generate a unique URL where the avatar can be downloaded.
    //     avatarUrl: require('util').format('%s/user/avatar/%s', sails.config.custom.baseUrl, req.CurrentUser.id),
    //     // Grab the first file and use it's `fd` (file descriptor)
    //     avatarFd: uploadedFiles[0].fd
    //   })
    //   .exec((error, result) => {
    //     if (error) {
    //       return ResponseService.json(500, res, 'Error: User avatar upload', error);
    //     }
    //     return ResponseService.json(200, res, 'User avatar upload successful', result);
    //   });
    // });
  },
  DownloadAvatar: async function (req, res) {
    sails.log(`AvatarController.DownloadAvatar()`);

    User.findOne({where: { 'id': req.params.id }}).exec((err, User) => {
      if (err) {
        return ResponseService.json(500, res, 'todo avatar download error', err);
      }

      if (!User) {
        // return res.notFound();
        return ResponseService.json(404, res, 'User not found', err);
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);
      if (fileAdapter) {
        try {
          // set the filename to the same file as the User uploaded
          res.set('Content-disposition', `attachment; filename='${User.DisplayName}_avatar'`);

          // Stream the file down
          fileAdapter.read(User.avatarFd)
            .on('error', (error) => {
              return ResponseService.json(404, res, 'User avatar download failed', error);
            })
            .pipe(res);
        } catch (error) {
          return ResponseService.json(500, res, 'User avatar download failed', error);
        }
      }
    });
  }
};
