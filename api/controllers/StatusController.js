/**
 * StatusController
 *
 * @description :: Server-side actions for handling status message requests.
 */

module.exports = {
  PostUserStatus: async function (req, res) {
    sails.log(`StatusController.PostUserStatus()`);

    var postData = {
      Message: req.body.message,
      Owner: req.CurrentUser.id
    };
    var statusRecord = await Status.create(postData).fetch();

    if (statusRecord) {
      try {
        var fullRecord = await Status.findOne({
          where: { 'id': statusRecord.id }
        }).populate('Owner');

        return ResponseService.json(201, res, 'Status message created successfully', fullRecord);
      }
      catch(error) {
        return ResponseService.json(500, res, 'Status message created, unable to fetch associated user data', error);
      }
    } else {
      return ResponseService.json(500, res, 'Status message could not be created');
    }
  },
  GetUserStatus: async function (req, res) {
    sails.log(`StatusController.GetUserStatus()`);

    // pagination offset
    var offset = req.params.offset ? req.params.offset : 0;

    var records = await Status.find({
		  where: { 'Owner': req.CurrentUser.id },
		  sort: [{ createdAt: 'DESC' }],
      limit: 5,
      skip: offset
    }).populate('Owner');

    if (records) {
      return ResponseService.json(200, res, 'Status posts fetched successfully', records);
    } else {
      return ResponseService.json(500, res, 'Status posts fetch failed');
    }
  },
  EditUserStatus: async function (req, res) {
    sails.log(`StatusController.EditUserStatus()`);

    try {
      var patchRecord = await Status.update({'id': req.body.postId}, {'Message': req.body.message}).fetch();
      return ResponseService.json(200, res, 'Status message updated successfully', patchRecord);
    } catch (error) {
      if (typeof(error) === UsageError) {
        return ResponseService.json(422, res, 'Status message formatted incorectly');
      } else {
        return ResponseService.json(500, res, 'Status message edit failed');
      }
    }
  },
  DeleteUserStatus: async function(req, res) {
    sails.log(`StatusController.EditUserStatus()`);
    if (req.params.id !== null && req.params.id.length > 0) {
      try {
        await Status.destroy({'id': req.params.id, 'Owner': req.CurrentUser.id});
        return ResponseService.json(200, res, 'Status message deleted successfully');
      } catch (error) {
        if (typeof(error) === UsageError) {
          return ResponseService.json(422, res, 'Status message delete failed');
        } else {
          return ResponseService.json(500, res, 'Status message delete failed');
        }
      }
    } else {
      return ResponseService.json(500, res, 'Status message delete failed, missing params');
    }
  },
  GetAllRecent: async function (req, res) {
    sails.log(`StatusController.GetAllRecent()`);

    try {
      var records = await Status.find({
        sort: [{ createdAt: 'DESC' }]
      }).populate('Owner').limit(50);

      return ResponseService.json(200, res, 'Status messages fetched successfully', records);
    } catch (error) {
      return ResponseService.json(500, res, 'Status messages fetch failed', error);
    }
  }
};
