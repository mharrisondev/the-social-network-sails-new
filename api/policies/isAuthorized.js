module.exports = function (req, res, next) {
  sails.log(`Policy:isAuthorized`);
  let token;

  if (req.headers && req.headers.authorization) {
    /* if token in headers */
    var parts = req.headers.authorization.split(' ');
    if (parts.length === 2) {
      var scheme = parts[0];
      var credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    } else {
      return ResponseService.json(401, res, 'Format is Authorization: Bearer [token]');
    }
    /* if token in query param (?xxx) */
  } else if (req.param('token')) {
    token = req.param('token');

    delete req.query.token;
  } else {
    return ResponseService.json(401, res, 'No authorization header was found');
  }

  JwtService.verify(token, (err, decoded) => {
    if (err) {
      return ResponseService.json(401, res, 'Invalid Token!');
    }
    req.token = token;
    User.findOne({id: decoded.id}).then((user) => {
      req.CurrentUser = user;
      next();
    });
  });
};
